<?php
    namespace App\Controller;

    use App\Controller\AppController;

    class HourController extends AppController
    {
	    var $table = false;
	    public function index(){
			//	Define Peru Time Zone
		    date_default_timezone_set("America/Lima");
			//  date_default_timezone_set("Asia/Tokyo");
			// 	date_default_timezone_set("Asia/Anadyr");
		    //	Define Hour and Minutes
		    $hour = date("H");
		    $minutes = date("i");
			//	Define variable to save the message
			$message = "";
			//	Create conditions to produce the message
			if (($hour >= 3 && $hour <= 11 ) && ($minutes >= 00 && $minutes <= 59)){
				$message = "Good morning";
			}elseif (($hour >= 12 && $hour <= 17 ) && ($minutes >= 00 && $minutes <= 59)){
				$message = "Good afternoon";
			}else {
				$message = "Good evening";
			}
			//	The message is sent through a set trigger
		    $this->set('message',$message);
	    }
    }
